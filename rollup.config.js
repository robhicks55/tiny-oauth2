import { join } from 'path';
import { terser } from 'rollup-plugin-terser';
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
import liveServer from 'rollup-plugin-live-server';
import env from 'dotenv';

env.config();

const mode = process.env.NODE_ENV;
const dev = mode === 'development';
const root = process.cwd();
const input = join(root, 'src', 'index.js');

const plugins = [ resolve(), commonjs() ];

const serverConfig = {
  input,
  plugins,
  output: {
    file: join(root, 'index.js'),
    format: 'cjs'
  }
};

const clientConfig = {
  input,
  plugins,
  output: {
    file: join(root, 'dist', 'tiny-oauth2.js'),
    format: 'es'
  }
};

const minClientConfig = {
  input,
  plugins: [ ...plugins, terser() ],
  output: {
    file: join(root, 'dist', 'tiny-oauth2.min.js'),
    format: 'es'
  }
};

const browserTestConfig = {
  input: join(root, 'test', 'tests.js'),
  plugins: [ ...plugins, liveServer({
    file: 'index.html',
    logLevel: 2,
    mount: [[ '/dist', './dist' ], [ '/src', './src' ], [ '/node_modules', './node_modules' ], [ '/test', './test' ]],
    open: false,
    port: 3001,
    root: './test',
    verbose: false,
    wait: 500
  }) ],
  output: {
    file: join(root, 'test', 'browser-test-bundle.js'),
    format: 'es'
  }
};

const serverTestConfig = {
  input: join(root, 'test', 'tests.js'),
  plugins,
  output: {
    file: join(root, 'test', 'server-test-bundle.js'),
    format: 'cjs'
  }
};

const config = dev
  ? [ serverConfig, clientConfig, browserTestConfig, serverTestConfig ]
  : [ serverConfig, clientConfig, minClientConfig ];

export default config;
