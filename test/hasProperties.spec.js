import { hasProperties } from '../src/hasProperties.js';

describe('hasProperties', () => {

  it('should return false if one property is missing', () => {
    const obj = {clientId: 'foo'};
    const re = hasProperties(obj, ['redirectUri']);
    expect(re).to.be.false;
  });
  it('should return true if all properties exist', () => {
    const obj = {clientId: 'foo', redirectUri: 'bar'};
    const re = hasProperties(obj, ['clientId', 'redirectUri']);
    expect(re).to.be.true;
  });
});
