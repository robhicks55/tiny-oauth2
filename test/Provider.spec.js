import { Provider } from '../dist/tiny-oauth2.js';

describe('Provider(options={})', () => {
  let provider = null;
  const googleConfig = {
    authenticateUrl: 'https://google.com/auth',
    clientId: 'google',
    redirectUri: 'http://foo.com/'
  };

  afterEach(() => {
    provider = null;
  });

  it('should return a provider instance', () => {
    provider = new Provider(googleConfig);
    expect(provider.clientId).to.be.equal('google');
  });

  describe('buildAuthenticateRequestUri()', () => {
    it('should return a valid authentication uri', () => {
      provider = new Provider(googleConfig);
      const uri = provider.buildAuthenticateRequestUri();
      expect(uri).to.contain('https://google.com/auth');
      expect(uri).to.contain('?client_id=google');
      expect(uri).to.contain('&redirect_uri=http');
      expect(uri).to.contain('&response_type=code');
      expect(uri).to.contain('&state=');
    });
  });
});
