export function hasProperties(obj = {}, fields = []) {
  return fields.filter(f => obj[f] !== undefined).length > 0;
}
