import { hasProperties } from './hasProperties.js';
import { uuid } from './uuid.js';
import TinyUri from 'tiny-uri';

export class Provider {
  constructor(options = {}) {
    if (hasProperties(options, [ 'authenticateUrl', 'clientId', 'redirectUri' ])) {
      this.authenticateUrl = options.authenticateUrl;
      this.clientId = options.clientId;
      this.redirectUri = options.redirectUri;
      this.scope = options.scope;
      this.state = uuid();
    } else throw new Error('missing authenticateUrl, clientId or redirectUri');
  }

  buildAuthenticateRequestUri() {
    const requestUri = new TinyUri(this.authenticateUrl);
    return requestUri.query.set({
      client_id: this.clientId,
      redirect_uri: this.redirectUri,
      response_type: 'code',
      scope: this.scope,
      state: this.state
    }).toString();
  }

  getAthenticationToken(ev) {
    console.log('ev', ev);
  }

  loginWithPopup(options = {}) {
    this.oauthWindow = window.open(this.buildAuthenticateRequestUri(), options.windowName, options.windowOptions);
    window.addEventListener('message', this.getAthenticationToken.bind(this));
  }

}
